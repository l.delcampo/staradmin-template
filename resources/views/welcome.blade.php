<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>La Estilista</title>
    
    <link href="//cdn.materialdesignicons.com/5.0.45/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/5.0.1/collection/components/icon/icon.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.6/css/flag-icon.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">
    <script src="https://kit.fontawesome.com/cd81f6f897.js" crossorigin="anonymous"></script>

    <link rel="shortcut icon" href="/images/favicon.png" />
  </head>
  <body>
    <div id="app">
        <div class="container-scroller">
            <nav-component></nav-component>
            <div class="container-fluid page-body-wrapper">
                <sidebar-component></sidebar-component>
                <div class="main-panel">
                    
                </div>           
            </div>
        </div>
    </div>
    <!-- container-scroller -->
    {{-- <!-- plugins:js -->
    <script src="/vendors/js/vendor.bundle.base.js"></script>
    <script src="/vendors/js/vendor.bundle.addons.js"></script> --}}
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="/js/app.js"></script>
    <script src="/js/shared/off-canvas.js"></script>
    <script src="/js/shared/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="/js/demo_1/dashboard.js"></script>
    <!-- End custom js for this page-->
  </body>
</html>